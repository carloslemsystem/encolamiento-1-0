<?php

function SpecialSplit($paquete2){
    $j=0; 
    $sCount=0;
    
    for ($i=0;$i<strlen($paquete2);$i++){
        if(($paquete2[$i]==' ' ||$paquete2[$i]=='-' || $paquete2[$i]=='+') && $i!=$j){
			$campos[$sCount]=trim(substr($paquete2, $j, $i-$j));
			if($campos[$sCount]==''){//mejora para firmware que viene con split + y con negativos (ejemplo +34+56+-45+7)
				continue;
			}
			//echo $campos[$sCount]." <br>";
			//var_dump($campos[$sCount], is_nan($campos[$sCount]));
			//echo "<br>";
			if(trim ($campos[$sCount])=="nan"){
			   // echo "detec";
				$campos[$sCount]=-1000;
			}
			$j=$i;
			$campos[$sCount]=(float)$campos[$sCount];
			$sCount++;
			
        }
    }
    
    $campos[$sCount]=substr($paquete2, $j, $i-$j);
    if(trim ($campos[$sCount])=="nan"){
       // echo "detec";
        $campos[$sCount]=-1000;
    }
    $campos[$sCount]=(float)$campos[$sCount];

    return ($campos);
}

/* funcion deprecada por que entro afuncion paquete_dato
function paquete_i($campos){
    include ("bd.php");
    $date1 = new DateTime("now");
    $date1 = $date1->format('Y-m-d H:i:s');



    if(count($campos)==8){
		$queEmpi="Insert into muestra_i (id_nodo, bateria, padre, lqi, potencia, correlacion, fecha,el) 
		values($campos[1],$campos[2],$campos[4],$campos[5],$campos[6],$campos[7],now(),$campos[3])";
		$resEmp = mysql_query($queEmpi, $conEmp);
	}else{
		$queEmpi="Insert into muestra_i (id_nodo, bateria, padre, lqi, potencia, correlacion, fecha,el) 
		values($campos[1],$campos[2],$campos[8],$campos[5],$campos[6],$campos[7],now(),$campos[3])";
		$resEmp = mysql_query($queEmpi, $conEmp);
    }
    
    $id_nodo=$campos[1];
    $direccion=$campos[3];

    //query para actualizar la fecha de los componentes con el paquete i
    $sql6="UPDATE control_riego set fecha='$date1',update_p=0 where id_nodo=$id_nodo";
    $consulta6=mysql_query($sql6, $conEmp);

    $ssql3 = "UPDATE nodo set direccion=$direccion where id_nodo=$id_nodo" ;
    $rs2 = mysql_query($ssql3,$conEmp);
    //sleep(5);
    //return();
    mysql_close($conEmp);

}*/

function dato_lemaqua($id_nodo){
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//   funcion para guardar en local los datos de lemaqua para la funcion de automatizacion
	//	 que se usan para agendar de manera automatica riegos por concepto de bajo nivel de estanques
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	include ("bd.php");
    $fecha=date('Y-m-d G:i:s'); 
	$date1=$fecha;
	
	
	
	$queEmp = "SELECT a0,a1,a2,a3,a4,a5,a6, b0,b1,b2,b3,b4,b5,b6, frecuencia, 
	nombre_tabla_muestra,nombre_tabla_muestra_mensual,tipo_nodo 
	from nodo where id_nodo=$id_nodo";
    
    $resEmp = mysql_query($queEmp, $conEmp);
    $totEmp = mysql_num_rows($resEmp);
    if($datatmp = mysql_fetch_array($resEmp)) {
	$a[0]=(float)$datatmp['a0'];
	$a[1]=(float)$datatmp['a1'];
	$a[2]=(float)$datatmp['a2'];
	$a[3]=(float)$datatmp['a3'];
	$a[4]=(float)$datatmp['a4'];
	$a[5]=(float)$datatmp['a5'];
	$a[6]=(float)$datatmp['a6'];
		
	$b[0]=(float)$datatmp['b0'];
	$b[1]=(float)$datatmp['b1'];
	$b[2]=(float)$datatmp['b2'];
	$b[3]=(float)$datatmp['b3'];
	$b[4]=(float)$datatmp['b4'];
	$b[5]=(float)$datatmp['b5'];
	$b[6]=(float)$datatmp['b6'];
	
		
	$frecuencia=(float)$datatmp['frecuencia'];
	$nombre_tabla_muestra=$datatmp['nombre_tabla_muestra'];
	$nombre_tabla_muestra_mensual=$datatmp['nombre_tabla_muestra_mensual'];
	$tipo_nodo=$datatmp['tipo_nodo'];
    }else{
        return;
    }
	
	if($tipo_nodo==7){
		//echo "hola";
		
			
			$queEmp = "SELECT referencia,id_sensor,umbral_superior,umbral_inferior,sms_activado,mail_activado,tipo,descripcion,tipo_paquete,c,d
					from sensor
					where id_nodo=$id_nodo and tipo_paquete='m' and visible=1 order by referencia asc";

			$resEmp = mysql_query($queEmp, $conEmp);
			$totEmp = mysql_num_rows($resEmp);
			//echo $queEmp."<br>";
			while($datatmp = mysql_fetch_array($resEmp)) {
				$referencia=$datatmp['referencia'];
				$id_sensor=$datatmp['id_sensor'];
				$tipo=$datatmp['tipo'];
				$c=(double)$datatmp['c'];
				$d=(double)$datatmp['d'];
				//echo "<br>";
				 if($tipo=='hs5'){
					$MuestraCalibrada= round((($campos[2+$referencia]*$a[$referencia]+$b[$referencia])*$c+$d),2);
					
					$resEmp43 = mysql_query("select t from conversion where N=$MuestraCalibrada and tipo='$tipo'", $conEmp);
					
					if(($datatmp43 = mysql_fetch_array($resEmp43))){ 
						
						$MuestraCalibrada=$datatmp43['t'];
						$ssql = "INSERT INTO $nombre_tabla_muestra (id_sensor,valor,fecha,update_p) 
								values('$id_sensor',$MuestraCalibrada,'$date1',1)";//echo $ssql."<br>";
						$ssqlmensual = "INSERT INTO $nombre_tabla_muestra_mensual (id_sensor,valor,fecha,update_p) 
								values('$id_sensor',$MuestraCalibrada,'$date1',1)";//echo $ssql."<br>";
						$rs1 = mysql_query($ssql,$conEmp); 
						$rsmensual = mysql_query($ssqlmensual,$conEmp);
						
						$ssql2 = "UPDATE sensor set valor=$MuestraCalibrada,fecha='$date1' where id_sensor=$id_sensor" ;
						$rs2 = mysql_query($ssql2,$conEmp);
							if($id_sensor == 17043){// id nodo 2509
								//automatico($id_sensor);
							}
					}
					
					
				}		
			}
			
			//$queEmp = "UPDATE mensaje_log SET  update_i = 1 WHERE id_log = $id_log ";
			//$resEmp = mysql_query($queEmp, $conEmp);
			
			//$queEmp = "delete from mensaje_log where update_p=1 and update_i=1 and id_log = $id_log";
			//$resEmp = mysql_query($queEmp, $conEmp);
			return;
		}else{
			return;
			//$queEmp = "UPDATE mensaje_log SET  update_i = 1 WHERE id_log = $id_log ";
			//$resEmp = mysql_query($queEmp, $conEmp);
			
			//$queEmp = "delete from mensaje_log where update_p=1 and update_i=1 and id_log = $id_log";
			//$resEmp = mysql_query($queEmp, $conEmp);
		}
	
	
	
	
	
}

function paquete_dato($PaqueteTipo,$paquete,$campos){
    
    include ("bd.php");
    $fecha=date('Y-m-d G:i:s'); 
    /*
    $queEmp_dato="select mensaje,tipo_paquete from mensaje_log where tipo_paquete='m' order by fecha desc limit 1";
    $resEmp = mysql_query($queEmp_dato, $conEmp);
    if($datatmp = mysql_fetch_array($resEmp)){
        $mensaje=$datatmp['mensaje'];
        $tipo_paquete_bd=$datatmp['tipo_paquete'];
    }
   
    $campos_bd=SpecialSplit($mensaje);
    $ultimo_numero_secuencia=$campos_bd[0];
    $numero_secuencia=$campos[0];
	

    if($ultimo_numero_secuencia==$numero_secuencia){
        return 4;
    }
	*/
	if($PaqueteTipo=='m'){
		$id_nodo_mensaje=$campos[1];
		dato_lemaqua($id_nodo_mensaje);
	}
	if($PaqueteTipo=='i'){
	$id_nodo=$campos[1];
    $direccion=$campos[3];
		$sql6="UPDATE control_riego set fecha='$fecha',update_p=0 where id_nodo=$id_nodo";
    	$consulta6=mysql_query($sql6, $conEmp);
	
	$ssql3 = "UPDATE nodo set direccion=$direccion where id_nodo=$id_nodo" ;

    $rs2 = mysql_query($ssql3,$conEmp);
	}
	
	
        $queEmp_dato="Insert into mensaje_log (mensaje, tipo_paquete, fecha) 
        values('$paquete','$PaqueteTipo','$fecha')";
        $resEmp = mysql_query($queEmp_dato, $conEmp);

        $queEmp_dato="Insert into mensaje_log_historico (mensaje, tipo_paquete, fecha) 
        values('$paquete','$PaqueteTipo','$fecha')";
        $resEmp = mysql_query($queEmp_dato, $conEmp);
    
        return 1;



}

function sensor_presostato($paquete2){
	include ("bd.php");	
	$campos_aux=SpecialSplit($paquete2);
	$pines_sol1 = 0;
	$pines_sol2 = 0;
	$pines_sol3 = 0;
	$pines_sol4 = 0;
	$variable_agua_termino = 0;
	$variable_agua_inicio = 0;
	$id_nodo_aux = $campos_aux[1];
	$variable_agua = 0;
	$iteracion = 0;
	$swAbierto = 0;
	$swCerrado = 0;
	$p0_cerrado = 0;
	$p1_cerrado = 0;
	$p2_cerrado = 0;
	$p0_abierto = 0;
	$p1_abierto = 0;
	$p2_abierto = 0;
	$date1 = new DateTime("now");
	$date1 = $date1->format('Y-m-d H:i:s');
	$mensaje = array();
	$queEmp = "SELECT a0,a1,a2,a3,a4,a5,a6, b0,b1,b2,b3,b4,b5,b6, frecuencia,tipo_nodo, 
	nombre_tabla_muestra,nombre_tabla_muestra_mensual from nodo where id_nodo=$id_nodo_aux and tipo_nodo=8";
	
	$resEmp = mysql_query($queEmp, $conEmp);
	$totEmp = mysql_num_rows($resEmp);
	if($datatmp = mysql_fetch_array($resEmp)) {
		$a[0]=(float)$datatmp['a0'];
		$a[1]=(float)$datatmp['a1'];
		$a[2]=(float)$datatmp['a2'];
		$a[3]=(float)$datatmp['a3'];
		$a[4]=(float)$datatmp['a4'];
		$a[5]=(float)$datatmp['a5'];
		$a[6]=(float)$datatmp['a6'];
		
		$b[0]=(float)$datatmp['b0'];
		$b[1]=(float)$datatmp['b1'];
		$b[2]=(float)$datatmp['b2'];
		$b[3]=(float)$datatmp['b3'];
		$b[4]=(float)$datatmp['b4'];
		$b[5]=(float)$datatmp['b5'];
		$b[6]=(float)$datatmp['b6'];
	
		
		$frecuencia=(float)$datatmp['frecuencia'];
		$tipo_nodo=$datatmp['tipo_nodo'];
		$nombre_tabla_muestra=$datatmp['nombre_tabla_muestra'];
		$nombre_tabla_muestra_mensual=$datatmp['nombre_tabla_muestra_mensual'];
	}else{
		exit;		
	}
	
	$sql = "select umbral_inferior,referencia,valor,fecha,descripcion,c,d,tipo from sensor where id_nodo = $id_nodo_aux and visible=1 order by referencia asc";
	$resEmp2 = mysql_query($sql, $conEmp);
	while($datatmp = mysql_fetch_array($resEmp2)) {
		
		$umbral_de_corte = $datatmp['umbral_inferior'];
		$referencia = $datatmp['referencia'];
		$valor = $datatmp['valor'];
		$c = $datatmp['c'];
		$d = $datatmp['d'];
		$tipo = $datatmp['tipo'];
		
		$MuestraCalibrada=round(($campos_aux[2+$referencia]*$a[$referencia]+$b[$referencia])*$c+$d);
		$resEmp43 = mysql_query("select t from conversion where N=$MuestraCalibrada and tipo='$tipo'", $conEmp);
		if(($datatmp43 = mysql_fetch_array($resEmp43))){
			$MuestraCalibrada=$datatmp43['t'];
		}else{
			exit;
		}
		
		if($umbral_de_corte >= $MuestraCalibrada){
			$swCerrado = 1;
			$estado_presostatos = 22;
			if($referencia == 3){
				//significa que estoy en el sol3
				$p0_cerrado = 16;
			}else if($referencia == 1){
				//significa que estoy en el sol1
				if($p1_cerrado == 8){
					$p1_cerrado = 9;
				}else{
					$p1_cerrado = 8;
				}
			}else if($referencia == 2){
				//significa que estoy en el sol2
				$p2_cerrado = 2;
			}else if($referencia == 4){
				//significa que estoy en el sol4
				if($p1_cerrado == 8){
					$p1_cerrado = 9;
				}else{
					$p1_cerrado = 1;
				}
			}
			//entro en el caso donde hay presion, por ende si estan regando
			//$pines = $pines.'';		
		}else{
			$swAbierto	= 1;	
			$estado_presostatos = 11;
			if($referencia == 3){
				//significa que estoy en el sol3
				$p0_abierto = 16;
			}else if($referencia == 1){
				//significa que estoy en el sol1
				if($p1_abierto == 8){
					$p1_abierto = 9;
				}else{
					$p1_abierto = 8;
				}
			}else if($referencia == 2){
				//significa que estoy en el sol2
				$p2_abierto = 2;
			}else if($referencia == 4){
				//significa que estoy en el sol4
				if($p1_abierto == 8){
					$p1_abierto = 9;
				}else{
					$p1_abierto = 1;
				}
			}
		}		
	}
	if($swAbierto == 1){
		array_push($mensaje,'1'.'+'.$id_nodo_aux.'+'.'11'.'+'.'11'.'+'.$p0_abierto.'+'.$p1_abierto.'+'.$p2_abierto.'+'.'11'.'+'.'0'.'+'.'0'.'+'.'0');	
	}
	if($swCerrado == 1){
		array_push($mensaje,'1'.'+'.$id_nodo_aux.'+'.'22'.'+'.'22'.'+'.$p0_cerrado.'+'.$p1_cerrado.'+'.$p2_cerrado.'+'.'22'.'+'.'0'.'+'.'0'.'+'.'0');	
	}	
	//<ack>indice:id_nodo:n_orden:estado:p0:p1:p2:tiempo:c0:c1:c2<ack>
	//exit;
	//este campo es para el id nodo
	//$campos[1]

	//este campo es para el estado de los presostatos
	//$campos[3]

	//referencia al sol3, que esta en el p0 con valor 16
	//$campos[4]

	//referencia al sol1 y sol4, que esta en el p1 con valor 8(sol1), 1(sol4) y 9(sol1 y sol4)
	//$campos[5]

	//referencia al sol2, que esta en el p2 con valor 2
	//$campos[6]
	//var_dump($mensaje);

	$campos=SpecialSplit($mensaje[0]);
	//var_dump($campos);
	$iteracion = count($mensaje); 
	for ($index_z = 1; $index_z <= $iteracion; $index_z++) {
		if ($campos[3] == 11) {
			//<ack>indice:id_nodo:n_orden:estado:p0:p1:p2:tiempo:c0:c1:c2<ack>
			if ($campos[4] == 16) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 1 WHERE id_nodo = $campos[1] and px = 16";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "select caseta from programacion_val where id_nodo = $campos[1] and px = 16 and estado = 1";
				$resEmp2 = mysql_query($sql, $conEmp);
				if (!($datatmp = mysql_fetch_array($resEmp2))) {
					$sql = "select nombre from control_val where id_nodo = $campos[1] and px = 16";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$nombre = $datatmp['nombre'];
					}
					$sql = "select direccion from nodo where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$direccion = $datatmp['direccion'];
					}
					$sql = "select equipo,caseta,id_empresa from control_riego where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$equipo = $datatmp['equipo'];
						$caseta = $datatmp['caseta'];
						$id_empresa = $datatmp['id_empresa'];
					}
					$newlipe = 500000 * $id_empresa;
					$sql = "select max(id_programacion_riego) from programacion_val where id_empresa = '$id_empresa'";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					if ($datatmp = mysql_fetch_array($consulta)) {
						$lipe = $datatmp['max(id_programacion_riego)'];

						if ($lipe < $newlipe) {
							$id_programacion_riego = $newlipe;
						} else {
							$id_programacion_riego = $lipe + 1;
						}
					} else {
						$id_programacion_riego = $newlipe;
					}
					$hoy = strtotime($date1);
					$minutos = 2;
					$minutos = $minutos * 60;
					$hoy += $minutos;
					$hoy = date("Y-m-d H:i:s", $hoy);
					$sql = "INSERT INTO programacion_val (px,id_programacion_riego,caseta,equipo,id_empresa,id_nodo,estado,p_fecha_ini,p_fecha_fin,nombre,r_fecha_ini,r_fecha_fin) 
					VALUES(16,$id_programacion_riego,$caseta,$equipo,$id_empresa,$campos[1],1,'$date1','$hoy','$nombre','$date1','$date1')";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					
					//echo "<ctr>"."0".":".$campos[1].":16:0:0:"."0".":11:".$direccion."</ctr>"; 
					//exit;
				}
			}
			if ($campos[5] == 8) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 1 WHERE id_nodo = $campos[1] and px = 8";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "select caseta from programacion_val where id_nodo = $campos[1] and px = 8 and estado = 1 ";
				$resEmp2 = mysql_query($sql, $conEmp);
				if (!($datatmp = mysql_fetch_array($resEmp2))) {
					$sql = "select nombre from control_val where id_nodo = $campos[1] and px = 8";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$nombre = $datatmp['nombre'];
					}
					$sql = "select direccion from nodo where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$direccion = $datatmp['direccion'];
					}
					$sql = "select equipo,caseta,id_empresa from control_riego where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$equipo = $datatmp['equipo'];
						$caseta = $datatmp['caseta'];
						$id_empresa = $datatmp['id_empresa'];
					}
					$newlipe = 500000 * $id_empresa;
					$sql = "select max(id_programacion_riego) from programacion_val where id_empresa = '$id_empresa'";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					if ($datatmp = mysql_fetch_array($consulta)) {
						$lipe = $datatmp['max(id_programacion_riego)'];

						if ($lipe < $newlipe) {
							$id_programacion_riego = $newlipe;
						} else {
							$id_programacion_riego = $lipe + 1;
						}
					} else {
						$id_programacion_riego = $newlipe;
					}
					$hoy = strtotime($date1);
					$minutos = 2;
					$minutos = $minutos * 60;
					$hoy += $minutos;
					$hoy = date("Y-m-d H:i:s", $hoy);
					$sql = "INSERT INTO programacion_val (px,id_programacion_riego,caseta,equipo,id_empresa,id_nodo,estado,p_fecha_ini,p_fecha_fin,nombre,r_fecha_ini,r_fecha_fin) 
					VALUES(8,$id_programacion_riego,$caseta,$equipo,$id_empresa,$campos[1],1,'$date1','$hoy','$nombre','$date1','$date1')";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					//echo "<ctr>"."0".":".$campos[1].":0:8:0:"."0".":11:".$direccion."</ctr>";
					//exit;
				}
			}
			if ($campos[5] == 1) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 1 WHERE id_nodo = $campos[1] and px = 1";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "select caseta from programacion_val where id_nodo = $campos[1] and px = 1 and estado = 1 ";
				$resEmp2 = mysql_query($sql, $conEmp);
				if (!($datatmp = mysql_fetch_array($resEmp2))) {
					$sql = "select nombre from control_val where id_nodo = $campos[1] and px = 1";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$nombre = $datatmp['nombre'];
					}
					$sql = "select equipo,caseta,id_empresa from control_riego where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$equipo = $datatmp['equipo'];
						$caseta = $datatmp['caseta'];
						$id_empresa = $datatmp['id_empresa'];
					}
					$sql = "select direccion from nodo where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$direccion = $datatmp['direccion'];
					}
					$newlipe = 500000 * $id_empresa;
					$sql = "select max(id_programacion_riego) from programacion_val where id_empresa = '$id_empresa'";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					if ($datatmp = mysql_fetch_array($consulta)) {
						$lipe = $datatmp['max(id_programacion_riego)'];

						if ($lipe < $newlipe) {
							$id_programacion_riego = $newlipe;
						} else {
							$id_programacion_riego = $lipe + 1;
						}
					} else {
						$id_programacion_riego = $newlipe;
					}
					$hoy = strtotime($date1);
					$minutos = 2;
					$minutos = $minutos * 60;
					$hoy += $minutos;
					$hoy = date("Y-m-d H:i:s", $hoy);
					$sql = "INSERT INTO programacion_val (px,id_programacion_riego,caseta,equipo,id_empresa,id_nodo,estado,p_fecha_ini,p_fecha_fin,nombre,r_fecha_ini,r_fecha_fin) 
					VALUES(1,$id_programacion_riego,$caseta,$equipo,$id_empresa,$campos[1],1,'$date1','$hoy','$nombre','$date1','$date1')";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					//echo "<ctr>"."0".":".$campos[1].":0:1:0:"."0".":11:".$direccion."</ctr>";
					//exit;
				}
			}
			if ($campos[5] == 9) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 1 WHERE id_nodo = $campos[1] and px = 1";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "select caseta from programacion_val where id_nodo = $campos[1] and px = 1 and estado = 1 ";
				$resEmp2 = mysql_query($sql, $conEmp);
				
				$sql = "select direccion from nodo where id_nodo = $campos[1]";
				$resEmp = mysql_query($sql, $conEmp);
				if ($datatmp = mysql_fetch_array($resEmp)) {
					$direccion = $datatmp['direccion'];
				}				
				if (!($datatmp = mysql_fetch_array($resEmp2))) {
					$sql = "select nombre from control_val where id_nodo = $campos[1] and px = 1";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$nombre = $datatmp['nombre'];
					}
					$sql = "select equipo,caseta,id_empresa from control_riego where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$equipo = $datatmp['equipo'];
						$caseta = $datatmp['caseta'];
						$id_empresa = $datatmp['id_empresa'];
					}
					$newlipe = 500000 * $id_empresa;
					$sql = "select max(id_programacion_riego) from programacion_val where id_empresa = '$id_empresa'";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					if ($datatmp = mysql_fetch_array($consulta)) {
						$lipe = $datatmp['max(id_programacion_riego)'];

						if ($lipe < $newlipe) {
							$id_programacion_riego = $newlipe;
						} else {
							$id_programacion_riego = $lipe + 1;
						}
					} else {
						$id_programacion_riego = $newlipe;
					}
					$hoy = strtotime($date1);
					$minutos = 2;
					$minutos = $minutos * 60;
					$hoy += $minutos;
					$hoy = date("Y-m-d H:i:s", $hoy);
					$sql = "INSERT INTO programacion_val (px,id_programacion_riego,caseta,equipo,id_empresa,id_nodo,estado,p_fecha_ini,p_fecha_fin,nombre,r_fecha_ini,r_fecha_fin) 
					VALUES(1,$id_programacion_riego,$caseta,$equipo,$id_empresa,$campos[1],1,'$date1','$hoy','$nombre','$date1','$date1')";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
				}
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 1 WHERE id_nodo = $campos[1] and px = 8";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "select caseta from programacion_val where id_nodo = $campos[1] and px = 8 and estado = 1 ";
				$resEmp2 = mysql_query($sql, $conEmp);
				if (!($datatmp = mysql_fetch_array($resEmp2))) {
					$sql = "select nombre from control_val where id_nodo = $campos[1] and px = 8";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$nombre = $datatmp['nombre'];
					}
					$sql = "select equipo,caseta,id_empresa from control_riego where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
						$equipo = $datatmp['equipo'];
						$caseta = $datatmp['caseta'];
						$id_empresa = $datatmp['id_empresa'];
					}
					$newlipe = 500000 * $id_empresa;
					$sql = "select max(id_programacion_riego) from programacion_val where id_empresa = '$id_empresa'";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					if ($datatmp = mysql_fetch_array($consulta)) {
						$lipe = $datatmp['max(id_programacion_riego)'];

						if ($lipe < $newlipe) {
							$id_programacion_riego = $newlipe;
						} else {
							$id_programacion_riego = $lipe + 1;
						}
					} else {
					$id_programacion_riego = $newlipe;
					}
					$hoy = strtotime($date1);
					$minutos = 2;
					$minutos = $minutos * 60;
					$hoy += $minutos;
					$hoy = date("Y-m-d H:i:s", $hoy);
					$sql = "INSERT INTO programacion_val (px,id_programacion_riego,caseta,equipo,id_empresa,id_nodo,estado,p_fecha_ini,p_fecha_fin,nombre,r_fecha_ini,r_fecha_fin) 
					VALUES(8,$id_programacion_riego,$caseta,$equipo,$id_empresa,$campos[1],1,'$date1','$hoy','$nombre','$date1','$date1')";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
				}
				//echo "<ctr>"."0".":".$campos[1].":0:9:0:"."0".":11:".$direccion."</ctr>";
				//exit;
			}
			if ($campos[6] == 2) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 1 WHERE id_nodo = $campos[1] and px = 2";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "select caseta from programacion_val where id_nodo = $campos[1] and px = 2 and estado = 1";
				$resEmp2 = mysql_query($sql, $conEmp);
				if (!($datatmp = mysql_fetch_array($resEmp2))) {
					$sql = "select nombre from control_val where id_nodo = $campos[1] and px = 2";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
					$nombre = $datatmp['nombre'];
					}
					$sql = "select equipo,caseta,id_empresa from control_riego where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
					$equipo = $datatmp['equipo'];
					$caseta = $datatmp['caseta'];
					$id_empresa = $datatmp['id_empresa'];
					}
					$sql = "select direccion from nodo where id_nodo = $campos[1]";
					$resEmp = mysql_query($sql, $conEmp);
					if ($datatmp = mysql_fetch_array($resEmp)) {
					$direccion = $datatmp['direccion'];
					}
					$newlipe = 500000 * $id_empresa;
					$sql = "select max(id_programacion_riego) from programacion_val where id_empresa = '$id_empresa'";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					if ($datatmp = mysql_fetch_array($consulta)) {
					$lipe = $datatmp['max(id_programacion_riego)'];

					if ($lipe < $newlipe) {
					$id_programacion_riego = $newlipe;
					} else {
					$id_programacion_riego = $lipe + 1;
					}
					} else {
					$id_programacion_riego = $newlipe;
					}
					$hoy = strtotime($date1);
					$minutos = 2;
					$minutos = $minutos * 60;
					$hoy += $minutos;
					$hoy = date("Y-m-d H:i:s", $hoy);
					$sql = "INSERT INTO programacion_val (px,id_programacion_riego,caseta,equipo,id_empresa,id_nodo,estado,p_fecha_ini,p_fecha_fin,nombre,r_fecha_ini,r_fecha_fin) 
					VALUES(2,$id_programacion_riego,$caseta,$equipo,$id_empresa,$campos[1],1,'$date1','$hoy','$nombre','$date1','$date1')";
					$consulta = mysql_query($sql, $conEmp) or die(mysql_error());
					//echo "<ctr>"."0".":".$campos[1].":0:0:2:"."0".":11:".$direccion."</ctr>";
					//exit;
				}
			}
		}else if ($campos[3] == 22) {
			//<ack>indice:id_nodo:n_orden:estado:p0:p1:p2:tiempo:c0:c1:c2<ack>
			$sql = "select direccion from nodo where id_nodo = $campos[1]";
			$resEmp = mysql_query($sql, $conEmp);
			if ($datatmp = mysql_fetch_array($resEmp)) {
				$direccion = $datatmp['direccion'];
			}
			if ($campos[4] == 16) {
			
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 16";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "UPDATE programacion_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 16 order by p_fecha_ini desc limit 1";
				$consulta = mysql_query($sql, $conEmp);
				//echo "<ctr>"."0".":".$campos[1].":16:0:0:"."0".":22:".$direccion."</ctr>";
				//exit;
			}
			if ($campos[5] == 8) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 8";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "UPDATE programacion_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 8 order by p_fecha_ini desc limit 1";
				$consulta = mysql_query($sql, $conEmp);
				//echo "<ctr>"."0".":".$campos[1].":0:8:0:"."0".":22:".$direccion."</ctr>";
				//exit;
			}
			if ($campos[5] == 1) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 1";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "UPDATE programacion_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 1 order by p_fecha_ini desc limit 1";
				$consulta = mysql_query($sql, $conEmp);
				//echo "<ctr>"."0".":".$campos[1].":0:1:0:"."0".":22:".$direccion."</ctr>";
				//exit;
			}
			if ($campos[5] == 9) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 1";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "UPDATE programacion_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 1 order by p_fecha_ini desc limit 1";
				$consulta = mysql_query($sql, $conEmp);

				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 8";
				$consulta = mysql_query($sql, $conEmp);
				$sql = "UPDATE programacion_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 8 order by p_fecha_ini desc limit 1";
				$consulta = mysql_query($sql, $conEmp);
				//echo "<ctr>"."0".":".$campos[1].":0:9:0:"."0".":22:".$direccion."</ctr>";
				//exit;
			}
			if ($campos[6] == 2) {
				$i = 0;
				$sql = "UPDATE control_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 2";
				$consulta = mysql_query($sql, $conEmp);
				$sql1 = "UPDATE programacion_val SET update_p = 0,estado = 2 WHERE id_nodo = $campos[1] and px = 2 order by p_fecha_ini desc limit 1";
				$consulta = mysql_query($sql1, $conEmp);
				//echo "<ctr>"."0".":".$campos[1].":0:0:2:"."0".":22:".$direccion."</ctr>";
				//exit;
			}
		} 
	}
}

function sincronizar_inteligente($input,$numeroaux,$iteracionaux){
	
	include ("bd.php");	
	$fecha=date('Y-m-d G:i:s');
	if($input=='entrar'){
		$sql = "select iteracion,numero from configuracion_empresa";
		if($consulta = mysql_query($sql, $conEmp)){
			while($datatmp = mysql_fetch_array($consulta)) {
				$iteracion = $datatmp['iteracion'];
				$numero = $datatmp['numero'];
			}
			return $iteracion.'/'.$numero;
		}else{
			return 'sindatos';
		}


	}else if($input=='error'){
		$error_msg = curl_error($ch);
		$nuevo_numero=20;
		$nueva_iteracion=0;
		
		if($iteracion!=0){
		$sql="UPDATE configuracion_empresa set numero = $nuevo_numero, iteracion=$nueva_iteracion";
		$consulta=mysql_query($sql, $conEmp);
		
		$sql="insert into historico_internet (fecha,numero) values('$fecha','$nuevo_numero')";
		$consulta=mysql_query($sql, $conEmp);
		}

	}else if($input=='aumento'){
		$nuevo_numero=$numeroaux+20;
		$nueva_iteracion=$iteracionaux+1;
		$sql="UPDATE configuracion_empresa set numero = $nuevo_numero, iteracion=$nueva_iteracion";
		$consulta=mysql_query($sql, $conEmp);
		
		$sql="insert into historico_internet (fecha,numero) values('$fecha','$nuevo_numero')";
		$consulta=mysql_query($sql, $conEmp);
	}

}

function guardar_direccion($campos){

	include ("bd.php");
    	$fecha=date('Y-m-d G:i:s');

	$id_nodo=$campos[1];
	$direccion=$campos[3];

	$sql6="UPDATE control_riego set fecha='$fecha' where id_nodo=$id_nodo";
	$consulta6=mysql_query($sql6, $conEmp);

	$ssql3 = "UPDATE nodo set direccion=$direccion where id_nodo=$id_nodo" ;
	$consulta3=mysql_query($ssql3, $conEmp);

}

?>
