<?php 

include ("funciones.php");

if(!empty($_GET['i'])){
	$paquete			= urlencode($_GET['i']); 
	$paquete2			= $_GET['i'];
	$PaqueteTipo		= "i";
	$campos				= SpecialSplit($paquete2);
	$respuesta			= paquete_dato($PaqueteTipo,$paquete,$campos);
	
    paquete_i($campos);
    echo "<ack>0:0:0</ack>";
    exit;
    
}else if(!empty($_GET['m'])) {


    $paquete			= urlencode($_GET['m']); //array con +
	$paquete2			= $_GET['m'];//array con espacios
	$PaqueteTipo		= "m";
    $campos				= SpecialSplit($paquete2);
	$respuesta			= paquete_dato($PaqueteTipo,$paquete,$campos);

	//********* funcion para simular paquetes 11 o 22 con presostatos enviando mensajes m 
	//********* descomentar para campos que lo requieran
					
						//sensor_presostato($paquete2);

	//************************************************************************************
	//************************************************************************************



	echo "<ack>$campos[1]:$campos[0]:$respuesta</ack>";
	exit;

}else if(!empty($_GET['r'])) {
    $paquete			= urlencode($_GET['r']);
    $paquete2			= $_GET['r'];
	$PaqueteTipo		= "r";
	$campos				= SpecialSplit($paquete2);
	$respuesta			= paquete_dato($PaqueteTipo,$paquete,$campos);

	echo "<ack>$campos[1]:$campos[0]:$respuesta</ack>";
	exit;

}else{
	exit; //salgo por que lo que llego no contiene un mensaje correspondiente a un M, R o I
}